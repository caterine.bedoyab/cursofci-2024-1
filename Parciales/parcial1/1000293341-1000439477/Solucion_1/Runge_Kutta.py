import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

class RKG: #Creamos la clase Runge Kutta 
    def __init__(self,h,N,G,A,B,f):
        """
        t0=initial time
        h=step size
        N= Order of the Runge Kutta
        G= coeficientes gamma, para el runge kutta 4 serían: G=[1/6,1/3,1/3,1/6]
        A= coeficientes alpha, para el runge kutta 4 serían A=[0,1/2,1/2,1]
        B=coeficientes beta, para el runge kutta sería el siguiente arreglo de arreglos: B=[[1/2],[0,1/2],[0,0,1]]
        """
        
        self.h=h #paso
        self.N=N #orden del runge kutta
        self.A=A # coeficientes alpha
        self.B=B #matriz de coeficientes beta
        self.G=G #coeficientes gamma
        self.f=f #función a resolver
     

    def RKG_Step(self,t0,y0):

        """
        y0=valor inicial
        f=función diferencial
        Este método realiza un paso para el valor inicial de y0 dado, la función f dada y los coeficientes creados en la clase.
        
        """
        
        
        K=np.zeros(self.N) ## Crea el arreglo para los k_i
        K[0]=self.h*self.f(t0,y0) ##Inicializa k_1

        for j in range(self.N-1): ## crea los k_(i+1) usando los coeficientes alpha y beta con todos los k_i anteriores
            suma=0
            for i in range(j+1):
                suma+=self.B[j][i]*K[i]
            K[j+1]=self.h*self.f(t0+self.h*self.A[j],y0+suma)
            
        S=0
        for i in range(self.N):
            S+=self.G[i]*K[i]
        return t0+self.h,y0+S
    
    

    def iter_RKG(self,n,t0,y0):
        F=[[t0],[y0]]
        for i in range(n):
            x_next = F[0][-1] + self.h
            y_next_rk = self.RKG_Step(F[0][-1], F[1][-1])[1]
            
            
            F[0].append(x_next)
            F[1].append(y_next_rk)
        return F
        

    
    def kutta_ploter(self,n,t0,y0,nombre): #Este método guarda la figura con los parámetros dados
        #k=np.arange(0,1+self.h,self.h)
        #f4=lambda x: np.exp(x)-x-1
        #K=f4(k)
        plt.plot(self.iter_RKG(n,t0,y0)[0],self.iter_RKG(n,t0,y0)[1])
        plt.title("Runge Kutta solution")
        plt.grid()
        plt.savefig(nombre)
        #plt.show()
        return
    
    def kutta_compare(self,n,t0,y0,nombre):
        analitic= lambda x: x - 1 + np.exp(-x)
        ttt=np.arange(0,(n*self.h)+self.h,self.h)
        mm=analitic(ttt)
        plt.plot(self.iter_RKG(n,t0,y0)[0],abs(self.iter_RKG(n,t0,y0)[1]-mm))
        plt.title("Error of Runge Kutta method using the Analitic solution")
        plt.grid()
        plt.savefig(nombre)
        #plt.show()

class RKG_3D(RKG): # Creamos la clase Runge-Kuta 3d que permite resolver sistema de ecuaciones acoplados
    def __init__(self,h,N,G,A,B,f,f1,f2):
        super().__init__(h,N,G,A,B,f)
        self.f1=f1 #Se agregan el otro par de funciones
        self.f2=f2
    
    def RKG_3D_step(self,t0,y0,r):  #Esta función da un paso para cada coordenada
        """
        y0= condiciones iniciales de x,y,z como un arreglo, ejm: y0=[0,1,3]
        r= coordenada en la que se va a realizar el paso del runge kutta: para x, se usa r=0, para y r=1, para z r=2
        """
        FF=np.array([self.f,self.f1,self.f2]) #Se crea un arreglo con las 3 funciones
        K=np.zeros(self.N) # Se crea un arreglo para guardar los valores de k
        K[0]=self.h*FF[r](t0,y0) #Se inicializa k1, ya que para crear los siguientes, k2,k3,... se necesitan los anteriores

        for j in range(self.N-1):
            suma=0
            for i in range(j+1):
                suma+=self.B[j][i]*K[i]
            y0[r]=y0[r]+suma
            K[j+1]=self.h*FF[r](t0+self.h*self.A[j],y0)  #Se crean los k2,k3,...,ks donde s es el orden del runge kutta
            
        S=0
        for i in range(self.N):
            S+=self.G[i]*K[i]  ##Se crea la suma de los coeficientes gamma_ multiplicados por los K_i
        return t0+self.h,y0[r]+S ##Se retorna un arreglo con el tiempo y la coordenada elegida actualizados.

    def iter_RKG_3D(self,n,t0,y0): ## Este método itera el método anterior, usando como condiciones iniciales el paso inmediatamente anterior
        """
        n= número de pasos
        y0= arreglo de condiciones iniciales y0=[x0,y0,z0]
        
        """
        F=[[t0],[y0[0]],[y0[1]],[y0[2]]]
        L=[y0[0],y0[1],y0[2]]
        for i in range(n):
            L[0]=self.RKG_3D_step(F[0][-1],L,0)[1]
            L[1]=self.RKG_3D_step(F[0][-1],L,1)[1]
            L[2]=self.RKG_3D_step(F[0][-1],L,2)[1]
            F[0].append(F[0][-1]+self.h)
            F[1].append(L[0])
            F[2].append(L[1])
            F[3].append(L[2])

        return F ## Retorna un arreglo F, donde F[0] es el tiempo, F[1],F[2],F[3] son los arreglos de x,y,z respectivamente

    def kutta_ploter_3D(self,n,t0,y0,nombre):
        #plt.plot(c.iter_RKG_3D(n,0)[0],c.iter_RKG_3D(10000,[0,0.01,0])[1])
        #plt.plot(c.iter_RKG_3D(10000,[0,0.01,0])[0],c.iter_RKG_3D(10000,[0,0.01,0])[2])
        #plt.plot(c.iter_RKG_3D(10000,[0,0.01,0])[0],c.iter_RKG_3D(10000,[0,0.01,0])[3])
        
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(self.iter_RKG_3D(n,t0,y0)[1],self.iter_RKG_3D(n,t0,y0)[2],self.iter_RKG_3D(n,t0,y0)[3])
        plt.savefig(nombre)
        #plt.show()
        return
        
    def kutta_ploter_grafs(self,n,t0,y0,nombre):
        fig, axs = plt.subplots(2, 2, figsize=(10, 8))

        # Plot data on each subplot
        axs[0, 0].plot(self.iter_RKG_3D(n,t0,y0)[1],self.iter_RKG_3D(n,t0,y0)[2] , color='r')
        axs[0, 0].set_title('x vs y')
        axs[0, 1].plot(self.iter_RKG_3D(n,t0,y0)[1], self.iter_RKG_3D(n,t0,y0)[3], color='g')
        axs[0, 1].set_title('x vs z')
        axs[1, 0].plot(self.iter_RKG_3D(n,t0,y0)[2], self.iter_RKG_3D(n,t0,y0)[3], color='b')
        axs[1, 0].set_title('y vs z')
    

        # Add labels to the x and y axes of each subplot
        for ax in axs.flat:
            ax.set_xlabel('x')
            ax.set_ylabel('y')

        # Adjust layout
        plt.tight_layout()

        # Show the plot
        plt.savefig(nombre)
        #plt.show()
        return
