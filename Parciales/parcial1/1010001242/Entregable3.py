import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from Entregable2 import RKG,RKGA #Importancion de las clases como librerias 

#Sistema de ecuaciones que define el atractor de Lorentz

def derivs(t, y, sigma, beta, rho):
    dxdt = sigma * (y[1] - y[0])
    dydt = y[0] * (rho - y[2]) - y[1]
    dzdt = y[0] * y[1] - beta * y[2]
    return np.array([dxdt, dydt, dzdt])

# Coeficientes para RK4
a = [[0, 0, 0, 0], [1/2, 0, 0, 0], [0, 1/2, 0, 0], [0, 0, 1, 0]]
b = [1/6, 1/3, 1/3, 1/6]
c = [0, 1/2, 1/2, 1]

# Parámetros específicos del problema 
sigma = 10
beta = 28
rho = 8/3

#Condiciones iniciales
x0 = 0.0
y01 = np.array([0.000000001, 0, 0]) #Array de condiciones iniciales
xf = 100                         # Valor final de tiempo ajustado a 100
h = 0.01 #paso

# Instancia de la clase y cálculo de la solución para casa caso 

#Caso1: x0=0 , y0=0 , z0=0, sigma = 10, beta = 28, rho = 8/3"
C1 = RKGA(derivs, x0, y01, xf, h, a, b, c, sigma, rho, beta)
X1, Y1 = C1.soluciones()
#Caso2: x0=0 , y0=0.01 , z0=0, sigma = 10, beta = 28, rho = 8/3"
y02=np.array([0.000000001, 0.01, 0])
C2 = RKGA(derivs, x0, y02, xf, h, a, b, c, sigma, rho, beta)
X2, Y2 = C2.soluciones()

#Caso3: x0=0 , y0=0 , z0=0, sigma = 16, beta = 45, rho = 4"
sigma1 = 16
beta1 = 45
rho1 = 4
y03=np.array([0.000000001, 0.0, 0])
C3 = RKGA(derivs, x0, y03, xf, h, a, b, c, sigma1, rho1, beta1)
X3, Y3 = C3.soluciones()
#Caso4: x0=0 , y0=0.001 , z0=0, sigma = 16, beta = 45, rho = 4"
y04=np.array([0.000000001, 0.001, 0])
C4 = RKGA(derivs, x0, y04, xf, h, a, b, c, sigma1, rho1, beta1)
X4, Y4 = C4.soluciones()



#Gráficos

def plot(str):
    #Función para generar gráficos específicos dependiendo del valor de 'str'.
    
    #Si la opcion es "Ci" se grafica lo solucion asociada al caso i-esimo como se estipulo  anteriormente.
    #Si la opcion es "3di" se grafica la solucion 3d asociada al caso i-esimo.
    
    Fig = plt.figure(figsize = (14,8))

    if str=="C1": 

        plt.suptitle("Graficos para el caso: x0=0 , y0=0 , z0=0, sigma = 10, rho = 28, beta = 8/3", fontsize=12)
        plt.subplot(1,4,1)
        plt.plot(Y1[:, 0],Y1[:, 1],'-.g',label='Caso #1')
        plt.grid(c="black",lw=0.1)      
        plt.xlabel("$X$ ")
        plt.ylabel(" $Y$ ")     
        #MARCO
        P = plt.gca()
        C="purple"
        P.spines['right'].set_color(C)
        P.spines['top'].set_color(C)
        P.spines['bottom'].set_color(C)
        P.spines['left'].set_color(C)  
        
         
        plt.subplot(1,4,2)
        plt.plot(Y1[:, 0],Y1[:, 2],'-.b',label='Caso #1')
        plt.grid(c="black",lw=0.1)      
        plt.xlabel("$X$ ")
        plt.ylabel(" $Z$ ")     
        #MARCO
        P = plt.gca()
        C="purple"
        P.spines['right'].set_color(C)
        P.spines['top'].set_color(C)
        P.spines['bottom'].set_color(C)
        P.spines['left'].set_color(C)       
        plt.subplot(1,4,3)
        plt.plot(Y1[:, 1],Y1[:, 2],'-.r',label='Caso #1')
        plt.grid(c="black",lw=0.1)      
        plt.xlabel("$Y$ ")
        plt.ylabel(" $Z$ ")     
        #MARCO
        P = plt.gca()
        C="purple"
        P.spines['right'].set_color(C)
        P.spines['top'].set_color(C)
        P.spines['bottom'].set_color(C)
        P.spines['left'].set_color(C)
        
        plt.show()
    if str=="C2":

        plt.suptitle("Graficos para el caso: x0=0 , y0=0.01 , z0=0, sigma = 10, rho = 28, beta = 8/3", fontsize=12)
        plt.subplot(1,4,1)
        plt.plot(Y2[:, 0],Y2[:, 1],'-.g',label='Caso #1')
        plt.grid(c="black",lw=0.1)      
        plt.xlabel("$X$ ")
        plt.ylabel(" $Y$ ")     
        #MARCO
        P = plt.gca()
        C="purple"
        P.spines['right'].set_color(C)
        P.spines['top'].set_color(C)
        P.spines['bottom'].set_color(C)
        P.spines['left'].set_color(C)       
        plt.subplot(1,4,2)
        plt.plot(Y2[:, 0],Y2[:, 2],'-.b',label='Caso #1')
        plt.grid(c="black",lw=0.1)      
        plt.xlabel("$X$ ")
        plt.ylabel(" $Z$ ")     
        #MARCO
        P = plt.gca()
        C="purple"
        P.spines['right'].set_color(C)
        P.spines['top'].set_color(C)
        P.spines['bottom'].set_color(C)
        P.spines['left'].set_color(C)       
        plt.subplot(1,4,3)
        plt.plot(Y2[:, 1],Y2[:, 2],'-.r',label='Caso #1')
        plt.grid(c="black",lw=0.1)      
        plt.xlabel("$Y$ ")
        plt.ylabel(" $Z$ ")     
        #MARCO
        P = plt.gca()
        C="purple"
        P.spines['right'].set_color(C)
        P.spines['top'].set_color(C)
        P.spines['bottom'].set_color(C)
        P.spines['left'].set_color(C)
        
        plt.show()
    if str=="C3":

        plt.suptitle("Graficos para el caso: x0=0 , y0=0.0 , z0=0, sigma = 16, beta = 4, rho = 45", fontsize=12)
        plt.subplot(1,4,1)
        plt.plot(Y3[:, 0],Y3[:, 1],'-.g',label='Caso #1')
        plt.grid(c="black",lw=0.1)      
        plt.xlabel("$X$ ")
        plt.ylabel(" $Y$ ")     
        #MARCO
        P = plt.gca()
        C="purple"
        P.spines['right'].set_color(C)
        P.spines['top'].set_color(C)
        P.spines['bottom'].set_color(C)
        P.spines['left'].set_color(C)       
        plt.subplot(1,4,2)
        plt.plot(Y3[:, 0],Y3[:, 2],'-.b',label='Caso #1')
        plt.grid(c="black",lw=0.1)      
        plt.xlabel("$X$ ")
        plt.ylabel(" $Z$ ")     
        #MARCO
        P = plt.gca()
        C="purple"
        P.spines['right'].set_color(C)
        P.spines['top'].set_color(C)
        P.spines['bottom'].set_color(C)
        P.spines['left'].set_color(C)       
        plt.subplot(1,4,3)
        plt.plot(Y3[:, 1],Y3[:, 2],'-.r',label='Caso #1')
        plt.grid(c="black",lw=0.1)      
        plt.xlabel("$Y$ ")
        plt.ylabel(" $Z$ ")     
        #MARCO
        P = plt.gca()
        C="purple"
        P.spines['right'].set_color(C)
        P.spines['top'].set_color(C)
        P.spines['bottom'].set_color(C)
        P.spines['left'].set_color(C)
        
        plt.show()
    if str=="C4":

        plt.suptitle("Graficos para el caso: x0=0 , y0=0.001 , z0=0, sigma = 16, beta = 4, rho = 45#", fontsize=12)
        plt.subplot(1,4,1)
        plt.plot(Y4[:, 0],Y4[:, 1],'-.g',label='Caso #1')
        plt.grid(c="black",lw=0.1)      
        plt.xlabel("$X$ ")
        plt.ylabel(" $Y$ ")     
        #MARCO
        P = plt.gca()
        C="purple"
        P.spines['right'].set_color(C)
        P.spines['top'].set_color(C)
        P.spines['bottom'].set_color(C)
        P.spines['left'].set_color(C)       
        plt.subplot(1,4,2)
        plt.plot(Y4[:, 0],Y4[:, 2],'-.b',label='Caso #1')
        plt.grid(c="black",lw=0.1)      
        plt.xlabel("$X$ ")
        plt.ylabel(" $Z$ ")     
        #MARCO
        P = plt.gca()
        C="purple"
        P.spines['right'].set_color(C)
        P.spines['top'].set_color(C)
        P.spines['bottom'].set_color(C)
        P.spines['left'].set_color(C)       
        plt.subplot(1,4,3)
        plt.plot(Y4[:, 1],Y4[:, 2],'-.r',label='Caso #1')
        plt.grid(c="black",lw=0.1)      
        plt.xlabel("$Y$ ")
        plt.ylabel(" $Z$ ")     
        #MARCO
        P = plt.gca()
        C="purple"
        P.spines['right'].set_color(C)
        P.spines['top'].set_color(C)
        P.spines['bottom'].set_color(C)
        P.spines['left'].set_color(C)
        
        plt.show()
    if str=="3d1":
        fig = plt.figure(figsize=(10, 7))
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(Y1[:, 0], Y1[:, 1], Y1[:, 2],"--g")
        ax.set_title('Atractor de Lorenz: Caso 1')
        ax.set_xlabel('X Axis')
        ax.set_ylabel('Y Axis')
        ax.set_zlabel('Z Axis')
        plt.show()
    if str=="3d2":
        fig = plt.figure(figsize=(10, 7))
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(Y2[:, 0], Y2[:, 1], Y2[:, 2],"--b")
        ax.set_title('Atractor de Lorenz: Caso 2')
        ax.set_xlabel('X Axis')
        ax.set_ylabel('Y Axis')
        ax.set_zlabel('Z Axis')
        plt.show()
    if str=="3d3":
        fig = plt.figure(figsize=(10, 7))
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(Y3[:, 0], Y3[:, 1], Y3[:, 2],"--r")
        ax.set_title('Atractor de Lorenz: Caso 3')
        ax.set_xlabel('X Axis')
        ax.set_ylabel('Y Axis')
        ax.set_zlabel('Z Axis')
        plt.show()
    if str=="3d4":
        fig = plt.figure(figsize=(10, 7))
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(Y4[:, 0], Y4[:, 1], Y4[:, 2],"--r")
        ax.set_title('Atractor de Lorenz: Caso 4')
        ax.set_xlabel('X Axis')
        ax.set_ylabel('Y Axis')
        ax.set_zlabel('Z Axis')
        plt.show()
       

        

     
    

#Uso de la funcion Plot()

#Graficas de xvsy, xvsz y yvsz para cada caso 
#plot("C1")  
plot("C2")
#plot("C3")
#plot("C4")

#Graficas en 3d para cada caso 
#plot("3d1")
#plot("3d2")
#plot("3d3")
#plot("3d4")

