import numpy as np
import matplotlib.pyplot as plt
from RKG import RKGA

if __name__ == '__main__':

    #Parámetros de la función de Lorenz 
    alpha = 10. 
    rho = 28.
    beta = 8/3.

    # Definición de las funciones de Lorentz f(t,Li)=dLi/dt=(dx_dt, dy_dt, dz_dt)
    def f(ti, Li):  

        f = np.zeros(3)
        f[0] = alpha * (Li[1] - Li[0])
        f[1] = Li[0] * (rho - Li[2]) - Li[1]
        f[2] = Li[0] * Li[1] - beta * Li[2]
        
        return f

    # Condiciones iniciales
    xo=0.0
    yo=0.01
    zo=0.0

    initial_conditions = [0, [xo , yo, zo]]  # [t_0, [x_0, y_0, z_0]]
    x_end = 100
    step_size = 0.01

    # Definición de constantes del método de Runge-Kutta 4
    order = 4
    constants = [[0, 0, 0, 0], [0.5, 0, 0, 0], [0, 0.5, 0, 0], [0, 0, 1, 0]]

    # Crear instancia de RGKA
    solver = RKGA(f, initial_conditions, x_end, step_size,order,constants)

    sol = solver.soluciones()

    fig, ax = plt.subplots(1,1, figsize=(6,5))
    ax.plot(sol[1][:, 0], sol[1][:, 1], label=f'$x_0={xo}$, $y_0={yo}$, $z_0={zo}$') # x vs y
    ax.legend()
    ax.grid()
    plt.title(f'Gráfico x vs y \nσ={alpha}, ρ={rho}, β={np.round(beta,2)}')
    plt.show()
    plt.close()

    fig, ax = plt.subplots(1,1, figsize=(6,5))
    ax.plot(sol[1][:, 0], sol[1][:, 2], label=f'$x_0={xo}$, $y_0={yo}$, $z_0={zo}$') # x vs z
    ax.legend()
    ax.grid()
    plt.title(f'Gráfico x vs z \nσ={alpha}, ρ={rho}, β={np.round(beta,2)}')
    plt.show()
    plt.close()

    fig, ax = plt.subplots(1,1, figsize=(6,5))
    ax.plot(sol[1][:, 1], sol[1][:, 2], label=f'$x_0={xo}$, $y_0={yo}$, $z_0={zo}$') # y vs z
    ax.legend()
    ax.grid()
    plt.title(f'Gráfico y vs z \nσ={alpha}, ρ={rho}, β={np.round(beta,2)}')
    plt.show()
    plt.close()

    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(sol[1][:, 0], sol[1][:, 1], sol[1][:, 2])
    ax.set_title(f'Lorenz 3D \nσ={alpha}, ρ={rho}, β={np.round(beta,2)} \n$x_0={xo}$, $y_0={yo}$, $z_0={zo}$')
    ax.set_xlabel('x(t)')
    ax.set_ylabel('y(t)')
    ax.set_zlabel('z(t)')
    plt.show()
    plt.close()
